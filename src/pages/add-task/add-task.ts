import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';

@IonicPage()
@Component({
  selector: 'page-add-task',
  templateUrl: 'add-task.html',
})
export class AddTaskPage {

  days = ['Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag', 'Zaterdag', 'Zondag']
  addTasksForm = { title: '', description: '', startTime: '', endTime: '', days: [] };

  constructor(public nav: NavController, public navParams: NavParams, private http: Http) {
    // this.http.get("assets/json/tasks.json")
    // .subscribe((res) => {
    //   let tasksByDate = {};
    //   let raw = res.json();
    //   raw.forEach((task) => {
    //     if (!tasksByDate[task.date]) {
    //       tasksByDate[task.date] = [];
    //     }
    //     tasksByDate[task.date].push(task);
    //   });
    //   for (let date in tasksByDate) {
    //     this.itemList.push({date: date, items: tasksByDate[date]});
    //   }
    // });
  }

  submitIdea() {
    // here we need the logic to save this idea in the database
    this.nav.pop();
  }

}
