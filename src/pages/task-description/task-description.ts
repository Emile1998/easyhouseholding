import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { MyTasksPage } from '../my-tasks/my-tasks';

@IonicPage()
@Component({
  selector: 'page-task-description',
  templateUrl: 'task-description.html',
})
export class TaskDescriptionPage {
  taskId: any;
  days = ['Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag', 'Zaterdag', 'Zondag']
  task = { title: '', description: '', startTime: '', endTime: ''};
  addTasksForm = { title: '', description: '', startTime: '', endTime: '', days: [] };

  constructor(public navCtrl: NavController, public navParams: NavParams, private http: Http) {
    this.taskId = navParams.get('taskId');

    this.http.get("assets/json/tasks.json")
    .subscribe((res) => {
      let tasksByDate = {};
      let raw = res.json();
      raw.forEach((task) => {
        if (task.id === this.taskId) {
          this.task.title = task.tasktitle;
          this.task.description = task.description;
          this.task.startTime = task.starttime;
          this.task.endTime = task.endtime;
          console.log(task);
        }
      });
    });
  }

  public afronden() {
    this.navCtrl.push(MyTasksPage);
  }

  
}
