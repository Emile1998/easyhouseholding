import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyAgendaPage } from './my-agenda';

@NgModule({
  declarations: [
    MyAgendaPage,
  ],
  imports: [
    IonicPageModule.forChild(MyAgendaPage),
  ],
})
export class MyAgendaPageModule {}
