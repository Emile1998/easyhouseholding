import { LoginPage } from '../login/login';
import { AuthService } from '../../providers/auth-service/auth-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Calendar } from '@ionic-native/calendar';
import { DateTime } from 'ionic-angular/components/datetime/datetime';

@IonicPage()
@Component({
  selector: 'page-my-agenda',
  templateUrl: 'my-agenda.html',
})
export class MyAgendaPage {
  debug;
  debug2;

  constructor(private nav: NavController, private auth: AuthService, private calendar: Calendar) {
    this.calendar.listEventsInRange(new Date('2017/12/12'), new Date('2018/01/13')).then((data) => {
      let events = [];
      data.forEach(event => {
        let preparedEvent = { "event_id" : event.event_id, "title" : event.title, "startTime" :  new Date(event.dtstart).toJSON(), "endTime" :  new Date(event.dtend).toJSON() };
        events.push(preparedEvent);
      });
      this.debug = events;
    })
  }

  importAgenda() {
    this.calendar.listCalendars().then((data) => {
      this.debug = data;
    })
  }

  public logout() {
    this.auth.logout().subscribe(succ => {
      this.nav.setRoot(LoginPage)
    });
  }

}
