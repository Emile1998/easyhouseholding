import { LoginPage } from '../login/login';
import { AuthService } from '../../providers/auth-service/auth-service';
import { Http } from '@angular/http';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AddTaskPage } from '../add-task/add-task';
import { TaskDescriptionPage } from '../task-description/task-description';

@IonicPage()
@Component({
  selector: 'page-my-tasks',
  templateUrl: 'my-tasks.html',
})
export class MyTasksPage {
  itemList = [];

  constructor(private nav: NavController, private auth: AuthService, private http: Http) {
    this.http.get("assets/json/tasks.json")
    .subscribe((res) => {
      let tasksByDate = {};
      let raw = res.json();
      raw.forEach((task) => {
        if (!tasksByDate[task.date]) {
          tasksByDate[task.date] = [];
        }
        tasksByDate[task.date].push(task);
      });
      for (let date in tasksByDate) {
        this.itemList.push({date: date, items: tasksByDate[date]});
      }
    });
  }

  public openTask(event, taskId) {
    this.nav.push(TaskDescriptionPage, {taskId: taskId.id});
  }

  public addTask() {
    this.nav.push(AddTaskPage)
  }

  public logout() {
    this.auth.logout().subscribe(succ => {
      this.nav.setRoot(LoginPage)
    });
  }

}
