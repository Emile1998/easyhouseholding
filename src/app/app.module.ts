import { HttpModule } from '@angular/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { LoginPage } from '../pages/login/login';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthService } from '../providers/auth-service/auth-service';
import { RegisterPage } from '../pages/register/register';
import { MyTasksPage } from '../pages/my-tasks/my-tasks';
import { MyAgendaPage } from '../pages/my-agenda/my-agenda';
import { AddTaskPage } from '../pages/add-task/add-task';
import { AgendaPage } from '../pages/agenda/agenda';

import { Calendar } from '@ionic-native/calendar';
import { NgCalendarModule } from 'ionic2-calendar';
import { MomentModule } from 'angular2-moment';
import { TaskDescriptionPage } from '../pages/task-description/task-description';



@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    AddTaskPage,
    MyTasksPage,
    RegisterPage,
    MyAgendaPage,
    AgendaPage,
    TaskDescriptionPage
  ],
  imports: [
    HttpModule,
    MomentModule,
    BrowserModule,
    NgCalendarModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    AddTaskPage,
    MyTasksPage,
    RegisterPage,
    MyAgendaPage,
    AgendaPage,
    TaskDescriptionPage
  ],
  providers: [
    Calendar,
    StatusBar,
    SplashScreen,
    AuthService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
